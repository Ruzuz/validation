import { useState } from 'react'; 
import PasswordStrengthBar from 'react-password-strength-bar';



const App = () => { 
const [num, setNum] = useState(''); 
// const { password } = num;
return ( 
	<div> 

	<p>Key pressed is: {num}</p> 

	<input autoComplete="off" type="password" onChange={(e) => {setNum(e.target.value)}}  /> 
  	<PasswordStrengthBar password={num} />
	</div> 
); 
}; 

export default App; 
